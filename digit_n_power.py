import time
start_time = time.time()

def sum_digit_n_power(n_power):
    working_numbers = [] 
    for i in range(2,10**n_power*2):
        sum_number = 0
        for number in str(i):
            sum_number += int(number)**n_power
        if sum_number == i:
            working_numbers.append(i)
    return sum(working_numbers)

print(sum_digit_n_power(5))
print(f"--- {(time.time() - start_time):.10f} seconds ---"  )